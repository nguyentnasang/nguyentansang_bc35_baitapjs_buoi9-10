function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var fullName = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;
  var nv = new NhanVien(
    taiKhoan,
    fullName,
    email,
    matKhau,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  );
  return nv;
}
function renderDSNV(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    contentHTML += `<tr>
        <td>${list[i].tk}</td>
        <td>${list[i].fullName}</td>
        <td>${list[i].email}</td>
        <td>${list[i].ngayLam}</td>
        <td>${list[i].chucVu}</td>
        <td>${list[i].tongluong()}</td>
        <td>${list[i].loainhanvien()}</td>
        <td>
        <button data-toggle="modal" data-target="#myModal" onclick='capNhatNV("${
          list[i].tk
        }")' class="btn btn-primary">cập nhật</button>
        <button onclick='xoaNv("${
          list[i].tk
        }")' class="mt-3 btn btn-danger">Xóa</button>
        </td>
        </tr>`;
  }

  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function showThongTinLenForm(list, i) {
  document.getElementById("tknv").value = list[i].tk;
  document.getElementById("name").value = list[i].fullName;
  document.getElementById("email").value = list[i].email;
  document.getElementById("password").value = list[i].matKhau;
  document.getElementById("datepicker").value = list[i].ngayLam;
  document.getElementById("luongCB").value = list[i].luongCB;
  document.getElementById("chucvu").value = list[i].chucVu;
  document.getElementById("gioLam").value = list[i].gioLam;
}
function findNV(idXepLoai, list) {
  if (idXepLoai == "hiển thị toàn bộ nhân viên") {
    renderDSNV(list);
  } else if (idXepLoai == "hiển thị nhân viên xuất sắt") {
    var newDSNV = [];

    for (var i = 0; i < list.length; i++) {
      if (dsnv[i].loainhanvien() == "xuất sắc") {
        var newDSNV = [];

        newDSNV.push(list[i]);
      }
    }
    renderDSNV(newDSNV);
  } else if (idXepLoai == "hiển thị nhân viên giỏi") {
    var newDSNV = [];

    for (var i = 0; i < list.length; i++) {
      if (list[i].loainhanvien() == "giỏi") {
        newDSNV.push(list[i]);
      }
    }
    renderDSNV(newDSNV);
  } else if (idXepLoai == "hiển thị nhân viên khá") {
    var newDSNV = [];

    for (var i = 0; i < list.length; i++) {
      if (list[i].loainhanvien() == "khá") {
        newDSNV.push(list[i]);
      }
    }
    renderDSNV(newDSNV);
  } else if (idXepLoai == "hiển thị nhân viên trung bình") {
    var newDSNV = [];

    for (var i = 0; i < list.length; i++) {
      if (list[i].loainhanvien() == "trung Bình") {
        newDSNV.push(list[i]);
      }
    }
    renderDSNV(newDSNV);
  }
}
