function kyTuToiDa(value) {
  var re = /^[a-z0-9_-]{4,6}$/;
  var result = re.test(value);
  if (result) {
    document.getElementById("tbTKNV").innerText = "";
    document.getElementById("tbTKNV").style.display = "block";
    return true;
  } else {
    document.getElementById("tbTKNV").innerText = "hãy nhập 4 đến 6 chữ Số";
    document.getElementById("tbTKNV").style.display = "block";
    return false;
  }
}
function kiemTraTK(idTK, list) {
  var index = list.findIndex(function (sv) {
    return sv.tk == idTK;
  });
  if (index == -1) {
    document.getElementById("tbTKNV").innerText = "";
    document.getElementById("tbTKNV").style.display = "block";
    return true;
  } else {
    document.getElementById("tbTKNV").innerText = "mã Tk bị trùng rồi kìa";
    document.getElementById("tbTKNV").style.display = "block";
    return false;
  }
}
function chinhapchucai(value) {
  if (value.length == 0) {
    document.getElementById("tbTen").innerText = "hãy nhập chữ cái";
    document.getElementById("tbTen").style.display = "block";
    return false;
  }
  var re = /^[a-zA-Z0-9 ]*$/;
  var result = re.test(value);
  if (result) {
    document.getElementById("tbTen").innerText = "";
    document.getElementById("tbTen").style.display = "block";
    return true;
  } else {
    document.getElementById("tbTen").innerText = "hãy nhập chữ cái";
    document.getElementById("tbTen").style.display = "block";
    return false;
  }
}

function validateEmail(value) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var result = re.test(value);
  if (result) {
    document.getElementById("tbEmail").innerText = "";
    document.getElementById("tbEmail").style.display = "block";
    return true;
  } else {
    document.getElementById("tbEmail").innerText =
      "email không đúng định dạng rồi!!!";
    document.getElementById("tbEmail").style.display = "block";
    return false;
  }
}
function validateMatKhau(value) {
  const re = /^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
  var result = re.test(value);
  if (result) {
    document.getElementById("tbMatKhau").innerText = "";
    document.getElementById("tbMatKhau").style.display = "block";
    return true;
  } else {
    document.getElementById("tbMatKhau").innerText =
      "Phải có 1 chữ hoa, 1 số, 1 ký tự đặc biệt và 8 ký tự trở lên";
    document.getElementById("tbMatKhau").style.display = "block";
    return false;
  }
}
function validateNgayLam(value) {
  const re =
    /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

  var result = re.test(value);
  if (result) {
    document.getElementById("tbNgay").innerText = "";
    document.getElementById("tbNgay").style.display = "block";
    return true;
  } else {
    document.getElementById("tbNgay").innerText =
      "ngày làm không được để trống đâu, đúng định dạng dd/mm/yyyy";
    document.getElementById("tbNgay").style.display = "block";
    return false;
  }
}
function validateLuongCB(value) {
  if (value >= 1000000 && value <= 20000000) {
    document.getElementById("tbLuongCB").innerText = "";
    document.getElementById("tbLuongCB").style.display = "block";
    return true;
  } else {
    document.getElementById("tbLuongCB").innerText = "lương từ 1tr - 20tr";
    document.getElementById("tbLuongCB").style.display = "block";
    return false;
  }
}
// tbChucVu
function validateChonCV(value) {
  if (value == "Chọn chức vụ") {
    document.getElementById("tbChucVu").innerText = "Hãy chọn chức vụ.";
    document.getElementById("tbChucVu").style.display = "block";
    return false;
  } else {
    document.getElementById("tbChucVu").style.display = "none";

    return true;
  }
}
function validateSoGioLam(value) {
  if (value >= 80 && value <= 200) {
    document.getElementById("tbGiolam").innerText = "";

    document.getElementById("tbGiolam").style.display = "none";
    return true;
  } else {
    document.getElementById("tbGiolam").innerText =
      "Số giờ làm trong tháng 80 - 200 giờ";
    document.getElementById("tbGiolam").style.display = "block";
    return false;
  }
}
