var dsnv = [];
var dataLocal = localStorage.getItem("DSNV");
if (dataLocal) {
  var dataRow = JSON.parse(dataLocal);
  dsnv = dataRow.map(function (item) {
    return new NhanVien(
      item.tk,
      item.fullName,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCB,
      item.chucVu,
      item.gioLam
    );
  });
  renderDSNV(dsnv);
}
// reset form
function resetForm() {
  document.getElementById("idForm").reset();
}
// saveLocalstore
function saveLocalStorage() {
  var dataJson = JSON.stringify(dsnv);
  localStorage.setItem("DSNV", dataJson);
}
//them nhan vien
document.getElementById("btnThem").onclick = function () {
  resetForm();
  document.getElementById("tknv").disabled = false;
};
document.getElementById("btnThemNV").onclick = function () {
  var newNv = layThongTinTuForm();
  var isValid = true;
  isValid &=
    chinhapchucai(newNv.fullName) &
    validateEmail(newNv.email) &
    validateMatKhau(newNv.matKhau) &
    validateNgayLam(newNv.ngayLam) &
    validateLuongCB(newNv.luongCB * 1) &
    validateChonCV(newNv.chucVu) &
    validateSoGioLam(newNv.gioLam);

  isValid &= kyTuToiDa(newNv.tk) && kiemTraTK(newNv.tk, dsnv);
  if (isValid) {
    dsnv.push(newNv);
    renderDSNV(dsnv);
    saveLocalStorage();
    resetForm();
  }
};
// xóa nhân viên
function xoaNv(idTk) {
  var index = dsnv.findIndex(function (nv) {
    return nv.tk == idTk;
  });
  dsnv.splice(index, 1);
  renderDSNV(dsnv);
  saveLocalStorage();
}
// show thong tin len form
function capNhatNV(idTK) {
  document.getElementById("tknv").disabled = true;
  var index = dsnv.findIndex(function (nv) {
    return nv.tk == idTK;
  });
  showThongTinLenForm(dsnv, index);
}
// cập nhật thông tin mới
document.getElementById("btnCapNhat").onclick = function () {
  var editNV = layThongTinTuForm();

  var index = dsnv.findIndex(function (nv) {
    return nv.tk == editNV.tk;
  });
  dsnv[index] = editNV;
  renderDSNV(dsnv);
  resetForm();
  document.getElementById("tknv").disabled = false;
  saveLocalStorage();
  alert("Hãy tắt form và xem KQ");
};
// tìm kiếm xếp loại nhân viên
document.getElementById("btnTimNV").onclick = function () {
  var xepLoai = document.getElementById("showTL").value;
  findNV(xepLoai, dsnv);
};
