function NhanVien(
  _taiKhoan,
  _fullName,
  _email,
  _matKhau,
  _ngayLam,
  _luongCB,
  _chucVu,
  _gioLam,
  _tongluong,
  _loainhanvien
) {
  this.tk = _taiKhoan;
  this.fullName = _fullName;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _ngayLam;
  this.luongCB = _luongCB;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.tongluong = function () {
    if (this.chucVu == "Sếp") {
      return this.luongCB * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luongCB * 2;
    } else if (this.chucVu == "Nhân viên") {
      return this.luongCB * 1;
    }
  };
  this.loainhanvien = function () {
    if (this.gioLam >= 192) {
      return "xuất sắc";
    } else if (this.gioLam >= 176 && this.gioLam < 192) {
      return "giỏi";
    } else if (this.gioLam >= 160 && this.gioLam < 176) {
      return "khá";
    } else if (this.gioLam <= 160) {
      return "trung Bình";
    }
  };
}
